#include <stdio.h>

#define SIZE 300

int main(int argc, char **argv)
{
    int c;
    char input[SIZE];
    int pos = 0;

    while ((c = getchar()) != EOF && pos < sizeof(input) - 1) {
      // if (c == '\n') continue;
      input[pos] = c;
      pos++;
    }
    input[pos] = '\0';
    int str_len = pos;

    for (int i = str_len ; i >= 0; i--){
      printf("%c", input[i]);
    }

    printf("\n");
    return (0);
}