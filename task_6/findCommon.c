#include "findCommon.h"

int main(int argc, char **argv){

  int list1[] = {0, 1, 4, 5, 90, 12, 56, 123, 593, 12390, 2349, 234, 23, 8384, 3942, 2039};
  int list2[] = {8, 34, 4, 56, 87, 12, 48, 93, 95, 50, 234, 23, 2039, 123};

  int numElem1 = sizeof(list1)/sizeof(list1[0]);
  int numElem2 = sizeof(list2)/sizeof(list2[0]);
  int resultSize = (numElem1 > numElem2 ? numElem2 : numElem1);
  int* result = (int*)calloc(resultSize, sizeof(int));

  err_t err = findCommon(list1, numElem1, list2, numElem2, result, resultSize);
  processError(err);

  printf("list1\n\t");
  printList(list1, numElem1);
  printf("\n");

  printf("list2\n\t");
  printList(list2, numElem2);
  printf("\n");

  printf("result\n\t");
  printList(result, resultSize);
  printf("\n");

  free(result);
  return 0;
}