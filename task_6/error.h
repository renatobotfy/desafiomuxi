#ifndef __ERROR_H__
#define __ERROR_H__

#include <stdio.h>

typedef enum error err_t;

enum error{
  OK,
  LIST1_NULL,
  LIST2_NULL,
  LISTS_EMPTY
};

void processError(err_t err){
  switch (err){
    case OK:
      printf("Success\n");
      break;

    case LIST1_NULL:
      printf("[error] List1 is null\n");
      break;

    case LIST2_NULL:
      printf("[error] List2 is null\n");
      break;

    case LISTS_EMPTY:
      printf("[error] Both lists are empty \n");
      break;

    default:
      printf("[error] Unknown error\n");
  }
}

#endif// __ERROR_H__