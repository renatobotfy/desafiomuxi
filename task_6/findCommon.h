#ifndef __FINDCOMMON_H__
#define __FINDCOMMON_H__

#include <stdlib.h>
#include <stdbool.h>
#include "error.h"

err_t findCommon(int *list1, int numElem1, int *list2, int numElem2, int *result, int resultSize);
void printList(int* list, int size);
int compare(const void* a, const void* b);
void swapLists(int *list1, int numElem1, int *list2, int numElem2);
bool binarySearch(int* listToSearch, int begin, int end, int elemToFind);

err_t findCommon(int *list1, int numElem1, int *list2, int numElem2, int *result, int resultSize){
  if (list1 == NULL) return LIST1_NULL;
  if (list2 == NULL) return LIST2_NULL;
  if (numElem1 == 0 && numElem2 == 0){
    //result = {};
    return LISTS_EMPTY;
  }

  qsort(list1, numElem1, sizeof(int), compare);

  if (numElem1 > numElem2){
    swapLists(list1, numElem1, list2, numElem2);
  }


  int positionFound = 0;
  for (int i = 0; i < numElem2; i++){
    if (binarySearch(list1, 0, numElem1, list2[i])){
      result[positionFound++] = list2[i];
    }
  }

  return OK;
}

bool binarySearch(int* listToSearch, int begin, int end, int elemToFind){

  if (end >= begin){

    int half = begin + (end - begin)/2;

    if (elemToFind == listToSearch[half]) return true;

    if (elemToFind > listToSearch[half]) return binarySearch(listToSearch, half + 1, end, elemToFind);

    return binarySearch(listToSearch, begin, half - 1, elemToFind);
  }
  return false;
}

void swapLists(int *list1, int numElem1, int *list2, int numElem2){
  int* temp = list1;
  list1 = list2;
  list2 = temp;
  
  int n_temp = numElem1;
  numElem1 = numElem2;
  numElem2 = n_temp;
  return;
}

void printList(int* list, int size){
  for (int i = 0; i < size; i++){
    printf("%d ", list[i]);
  }
    printf("\n");
}

int compare(const void* a, const void* b){
     int int_a = * ( (int*) a );
     int int_b = * ( (int*) b );

     if ( int_a == int_b ) return 0;
     else if ( int_a < int_b ) return -1;
     else return 1;
}

#endif// __FINDCOMMON_H__