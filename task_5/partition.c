#include "linkedList.h"
#include "partition.h"

int seed[] = {3, 7, 0, 1, 8, 5, 6, 4, 2, 9};

int main(int argc, char **argv){
  Head* listHead = newLinkedList();

  for (int i = 0; i < 10; i++){
    pushNode(listHead, seed[i]);
  }

  printLinkedList(listHead);
  listPartition(&listHead, 7);
  printLinkedList(listHead);

  free(listHead);
  return 0;
}