#ifndef __LINKEDLIST_H__
#define __LINKEDLIST_H__

#include <stdlib.h>
#include <stdio.h>

typedef struct node Node;
typedef struct head Head;
Head* newLinkedList();
Node* createNode(int id, int elem, Node* prev); // private
void pushNode(Head* n_node, int value);
//void listPartition(SingleLinkedListOfIntsNode **head, int x);
void printLinkedList(Head* n_node);
void printLinkedListBackwards(Head* head);

struct head {
  int size;
  Node* next_node;
  Node* last_node;
};

struct node {
  int value;
  int id;
  Node* next_node;
  Node* prev_node;
};

Node* createNode(int id, int elem, Node* prev){
  Node* n_node = (Node*)calloc(1, sizeof(Node));
  n_node->id = id;
  n_node->value = elem;
  n_node->prev_node = prev;
  n_node->next_node = NULL;

  return n_node;
}

Head* newLinkedList(){
  Head* n_head = (Head*)calloc(1, sizeof(Head));
  n_head->size = 0;
  return n_head;
}

void pushNode(Head* head, int value){
  Node* current = head->next_node;
  Node* prev = NULL;

  if (head->size == 0){
    head->next_node = createNode(0, value, NULL);
    head->last_node = head->next_node;
  }
  else{
    while(current != NULL){
      prev = current;
      current = current->next_node;
    }
    prev->next_node = createNode(prev->id+1, value, prev);
    head->last_node = prev->next_node;
  }

  head->size += 1;
  return;
}

void printLinkedList(Head* head){
  Node* current = head->next_node;

  while (current != NULL){
    printf("%d ", current->value);
    current = current->next_node;
  }
  printf("\n");
}

void printLinkedListBackwards(Head* head){
  Node* current = head->last_node;

  while (current != NULL){
    printf("%d ", current->value);
    current = current->prev_node;
  }
  printf("\n");
}
#endif// __LINKEDLIST_H__
