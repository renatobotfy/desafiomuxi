#ifndef __PARTITION_H__
#define __PARTITION_H__

#include "linkedList.h"

void listPartition(Head **head, int x);
void swap(Node* a, Node* b);

void listPartition(Head **head, int x){
  printf("partitioning around %d\n", x);
  Node* left;
  Node* right;

  if ((*head) == NULL || (*head)->size == 1) return;

  left = (*head)->next_node;
  right = (*head)->last_node;

  while(left->id < right->id){
    while(left->value < x){
      left = left->next_node;
    }

    while(right->value > x){
      right = right->prev_node;
    }

    swap(left, right);
  }

  return;
}

void swap(Node* a, Node* b){
  if (a->value == b->value) return;
  int temp;

  temp = a->value;
  a->value = b->value;
  b->value = temp;

  return;
}

#endif// __PARTITION_H__