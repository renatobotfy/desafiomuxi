#include "stringSplit.h"

int main(int argc, char **argv)
{
    int c;
    char input[SIZE];
    int pos = 0;

    while ((c = getchar()) != EOF && pos < sizeof(input) - 1) {
      input[pos] = c;
      pos++;
    }
    input[pos] = '\0';
    int str_len = pos;

    char** response;
    response = stringSplit(input, '/', &str_len);

    for (char** words = response; *words; words++)
      printf("%s\n", *words);

    printf("\n");
    return (0);
}
