#ifndef __STRINGSPLIT_H__
#define __STRINGSPLIT_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE 300

char** stringSplit (const char *text, char separator, int *length);

char** stringSplit (const char *text, char separator, int *length){
  char** ret;

  int old_pos = 0;
  int total = 0;
  for (int pos = 0; pos <= *length; pos++){
    if (text[pos] == separator){
      int size = pos - old_pos;
      if (size < 1){
        old_pos = pos;
        continue;
      }

      ret = realloc(ret, (total + 1)*sizeof(char*));
      char* buffer = (char*)calloc(size+1, sizeof(char));
      strncpy(buffer, &text[old_pos] + 1, size - 1);
      ret[total] = buffer;
      old_pos = pos;
      total++;
    }
  }

  ret[total] = NULL;
  return ret;
} 
 
#endif // __STRINGSPLIT_H__