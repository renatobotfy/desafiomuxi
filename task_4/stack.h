#ifndef __STACK_H__
#define __STACK_H__

#include <stdio.h>
#include <stdlib.h>

typedef struct stack Stack;
Stack *stackNew (void);
void stackFree (Stack *p);
void stackPush (Stack *p, int elem);
int stackPop (Stack *p);
int stackEmpty (Stack *p);
void stackRemoveEven (Stack *p);

struct stack{
  int size;
  int* data;
};

void stackRemoveEven (Stack *p){
  Stack* n_stack = stackNew();

  int check;
  while(p->size > 0){
    check = stackPop(p);
    if (check%2==0) continue;
    stackPush(n_stack, check);
  }

  while(n_stack->size > 0){
    stackPush(p, stackPop(n_stack));
  }

  free(n_stack);
  return;
}

Stack* stackNew(){
  Stack* n_stack = (Stack*)calloc(1, sizeof(Stack));
  n_stack->size = 0;
  n_stack->data = NULL;
  return n_stack;
}

int stackPop (Stack *p){
  if (p->size <= 0){
    return -2;
  }

  p->size -= 1;
  int data = p->data[p->size];
  p->data = realloc(p->data, p->size*sizeof(Stack));

  return data;
}

void stackPush (Stack *p, int elem){
  p->data = realloc(p->data, (p->size + 1)*sizeof(int));
  p->data[p->size++] = elem;
  return;
}

#endif// __STACK_H__