#include "stack.h"

void printStack(Stack* p);

int main(int argc, char **argv){

  Stack* n_stack = stackNew();

  for (int i = 0; i < 10; i++){
    stackPush(n_stack, i);
  }

  printf("Stack before: ");
  printStack(n_stack);
  stackRemoveEven(n_stack);
  printf("Stack after: ");
  printStack(n_stack);

  free(n_stack);
  return 0;
}

void printStack(Stack* p){
  printf("size: %d, values: ", p->size);
  for (int i = p->size-1; i >= 0 ; i--){
    printf("%d ", p->data[i]);
  }
  printf("\n");
  return;
}