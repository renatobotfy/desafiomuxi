#include "sudoku.h"

int main(int argc, char **argv)
{
    char c;
    int input[SIZE][SIZE];
    int pos = 0;

    while ((c = getchar()) != EOF && pos < sizeof(input)) {
      if (c == '\n' || c == ' ') continue;
      input[(int)(pos/9)][pos%9] = c - '0';
      pos++;
    }

    // print_Arr(SIZE, input); 

    // check lines, columns and blocks
    bool success = true;
    success &= (check_Lines(SIZE, input) &&
                check_Columns(SIZE, input) &&
                check_Blocks(SIZE, input));

    if (success) printf("Yoo Hoo \n\t Sudoku OK!\n");
    else printf("Sorry \n\t try again :)\n");
    printf("\n");
    return (0);
}
