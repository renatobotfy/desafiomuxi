#ifndef __SUDOKU_H__
#define __SUDOKU_H__

#include <stdbool.h>
#include <stdio.h>

#define SIZE 9

void print_Arr(int size, int input[size][size]);
bool check_Line(int size, int *input);
bool check_Lines(int size, int input[size][size]);
bool check_Column(int size, int input[size][size], int column);
bool check_Columns(int size, int input[size][size]);
bool check_Block(int line, int col, int size, int input[size][size]);
bool check_Blocks(int size, int input[size][size]);

void print_Arr(int size, int input[size][size]){
  for (int i = 0 ; i < size*size; i++){
    printf("%d ", input[(int)(i/9)][i%9]);
  }
}

bool check_Lines(int size, int input[size][size]){
  bool response = true;
  for (int line = 0; line < 9; line++){
    response &= check_Line(SIZE, input[line]);
    if (!response) break;
  }
  return response;
}

bool check_Line(int size, int* input){
  bool response = true;
  for (int check = 1; check <= size; check++){
    bool present = false;
    for (int i = 0; i < size; i++){
      if (check == input[i]) {
        present = true;
        break;
      }
    }
    response &= present;
    if (!response) break;
  }
  return response;
}

bool check_Columns(int size, int input[size][size]){
  bool response = true;
  for (int column = 0; column < size; column++){
    response &= check_Column(size, input, column);
    if (!response) break;
  }
  return response;
}

bool check_Column(int size, int input[size][size], int column){
  bool response = true;
  for (int check = 1; check <= size; check++){
    bool present = false;
    for (int i = 0; i < size; i++){
      if (check == input[i][column]) {
        present = true;
        break;
      }
    }
    response &= present;
    if (!response) {
      break;
    }
  }
  return response;
}

bool check_Blocks(int size, int input[size][size]){
  bool response = true;
  for (int line = 0; line < size; line+=3){
    for (int col = 0; col < size; col+=3){
      response &= check_Block(line, col, size, input);
      if (!response) break;
    }
    if (!response) break;
  }
  return response;
}

bool check_Block(int line, int col, int size, int input[size][size]){
  bool response = true;
  for (int check = 1; check <= 9; check++){
    bool present = false;
    for (int i = 0; i < 3; i++){
      for (int j = 0; j < 3; j++){
        if (check == input[line+i][col+j]){
          present = true;
          break;
        }
      }
      if (present) break;
    }
    response &= present;
    if (!response) break;
  }
  return response;
}

#endif // __SUDOKU_H__